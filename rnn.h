#ifndef RNN_H
#define RNN_H

#include <stddef.h>

#ifndef RNN_TRAINED
#define RNN_TRAINED "/usr/local/share/rnn/sample_brain"
#endif

extern int vflag;

#define verprintf(...) if (vflag) printf(__VA_ARGS__)

char *colorname(int i);
void print_xresources(float in[]);
void usage(char *name);
int rnn_run(char *input, char *brain);
void *xmalloc(size_t n);

#endif
