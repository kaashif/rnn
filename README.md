rnn
===
rnn is a program that generates Xresources color schemes from
images.

Building
--------
Use any POSIX-compliant make:

	$ make

Running
-------
Train the neural net on some data. Here, we use the urnnputs
submodule's data (none of it is provided by me):

	$ ./rnn -t -d inputs/data -f brain

The `brain` file is where the neural net will be dumped.

Provide an image to generate a color scheme for, here we use
`tests/0001.png`.

	$ ./rnn -f brain tests/0001.png

`rnn` will print the finished X resources file to stdout.

Read the man page for a detailed explanation of what the flags do and
some more examples.

Output Examples
---------------
Here are some examples:


| Input                   | Output (with sample brain)  |
|-------------------------|-----------------------------|
| ![](/images/photo1.png) | ![](/images/photo1_rnn.png) |
| ![](/images/photo2.png) | ![](/images/photo2_rnn.png) |
| ![](/images/photo3.png) | ![](/images/photo3_rnn.png) |

What does rnn mean?
-------------------
It stands for ricing neural network.

Is this a fork of [urnn](https://github.com/nixers-projects/urnn)?
------------------------------------------------------------------
No, rnn is a clone but not a fork. It shares no code with urnn and is
under a free license (ISC, see below or in the LICENSE file).

However, the inputs used to generate the sample brain are taken from
[urnnputs](https://github.com/nixers-projects/urnnputs). These assets
are not really included in this repository (it's a submodule) and you
could generate a neural network with any other inputs, so this
repository has no licensing issues.

License
-------
Copyright (c) 2017 Kaashif Hymabaccus

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

The code in the `colors` directory is not mine. It is provided under
the license you can find in the `colors/LICENSE` file.
