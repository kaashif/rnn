#ifndef SIN_COLORS_H
#define SIN_COLORS_H

/* See LICENSE file for copyright and license details. */

#include <stddef.h>
#include "tree.h"

struct point {
    int x;
    int y;
    int z;
    long long freq;
    struct cluster *c;
    RB_ENTRY(point) e;
};

struct cluster {
    struct point center;
    size_t nelems;
    struct {
        long long nmembers;
        long long x, y, z;
    } tmp;
};


void parseimg(char *, void (*)(int, int, int));
void fillpoints(int, int, int);
void initclusters(size_t);
void initcluster_greyscale(struct cluster *, int);
void process();

extern int eflag;
extern size_t nclusters;
extern void (*initcluster)(struct cluster *, int);
extern size_t initspace;
extern struct cluster *clusters;
RB_HEAD(pointtree, point) pointhead;

#endif
