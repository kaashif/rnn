#!/usr/local/bin/zsh

# This script regenerates the examples with rnn

rm *.fin.png *.resources

for photo in photo*.png; do
	local resources=${photo/png/resources}
	rnn $photo > $resources
	local scr=${photo/png/sh}
	local fin=${photo/png/fin.png}
	echo "convert -size 50x450 xc:skyblue \\" > $scr

	local counter=0

	cut -d' ' -f2 < $resources | while read l; do
		echo "-fill \"$l\" -draw \"rectangle 0,$(( counter * 25 )) 50,$(( 25 + counter * 25 ))\" \\" >> $scr
		(( counter = counter + 1 ))
	done

	echo "$fin" >> $scr

	sh $scr
	rm $scr
done
