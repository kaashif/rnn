.POSIX:

PROG=	rnn
SRCS=	main.c
OBJS=	$(SRCS:.c=.o)

TEST_PROG=	rnn_test
TEST_SRCS=	tests/main.c tests/test_colors.c
TEST_OBJS=	$(TEST_SRCS:.c=.o)

BOTH_SRCS=	rnn.c train.c colors.c colors/png.c colors/colors.c
BOTH_OBJS=	$(BOTH_SRCS:.c=.o)

CC=		cc
CFLAGS=		-std=c99 -Wall -Wextra -I/usr/local/include -g
LDFLAGS+=	-L/usr/local/lib -lpng -lfann
PREFIX=		/usr/local

all: $(PROG) $(TEST_PROG)

$(PROG): $(OBJS) $(BOTH_OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS) $(BOTH_OBJS) $(LDFLAGS)

$(TEST_PROG): $(TEST_OBJS) $(BOTH_OBJS)
	$(CC) $(CFLAGS) -o $(TEST_PROG) $(TEST_OBJS) $(BOTH_OBJS) $(LDFLAGS)

.c.o:
	$(CC) $(CFLAGS) -o $@ -c $<

.PHONY: clean install uninstall test

clean:
	rm -f $(PROG) $(OBJS) $(TEST_PROG) $(TEST_OBJS) $(BOTH_OBJS)

install: $(PROG)
	install -m 0755 $(PROG) $(PREFIX)/bin
	mkdir -p $(PREFIX)/share/rnn
	install -m 0644 sample_brain $(PREFIX)/share/rnn

uninstall:
	rm -f $(PREFIX)/bin/$(PROG)
	rm -rf $(PREFIX)/share/rnn

test: $(TEST_PROG)
	./$(TEST_PROG)
