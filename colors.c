#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "colors.h"
#include "rnn.h"
#include "colors/tree.h"
#include "colors/colors.h"

/* Uses sin's colors code to extract 10 rgb colors from the
 * input_file, assumed to be a PNG.
 *
 * `output` is assumed to be an array at least 30 big. Each color is
 * stored as three floats. e.g. #ff2700 is stored as three values:
 * ((float)0xff) / 255.0f and so on, one after another.
 */

int
extract_colors(char *input_file, float output[]) {
    FILE *f = fopen(input_file, "r");
    if (!f) {
        perror("fopen");
        return -1;
    }
    /* This is essentially the main() from the original
     * colors/colors.c */
    RB_INIT(&pointhead);
    eflag = 1;
    nclusters = 10;
    parseimg(input_file, fillpoints);
    initcluster = initcluster_greyscale;
    initspace = 256;
    initclusters(nclusters);
    process();

    if (fclose(f)) {
        perror("fclose");
        return -1;
    }

    int ints[30];

    int i;
    for (i = 0; i < 10; i++) {
        ints[3*i] = clusters[i].center.x;
        ints[3*i+1] = clusters[i].center.y;
        ints[3*i+2] = clusters[i].center.z;
    }

    for (i = 0; i < 30; i++) {
        output[i] = (float)ints[i]/255.0f;
    }
    return 0;
}

/* Extracts each of 18 colors from an Xresources formatted file (in
 * the same format) as those in the inputs/ submodule.
 *
 * `output` is assumed to be an array of size >=54. The colors are
 * encoded in the same way as `extract_colors`.
 */

int
extract_resources(char *input_file, float output[]) {
    FILE *f = fopen(input_file, "r");
    if (!f) {
        perror("fopen");
        return -1;
    }
    char *line = xmalloc(256);
    size_t linesize = 256;
    ssize_t linelen;
    int outputptr = 0;
    while ((linelen = getline(&line, &linesize, f)) != -1) {
        /* The line looks like this:
               *.colorname: #000000\n
               ^
               line
         */
        size_t i;

        if (*line == '!' || *line == '\n') {
            /* It's a comment or empty line */
            continue;
        }

        for (i = 0; i < linesize && line[i] != '#'; i++);

        if (i == linesize) {
            /* We didn't find a # */
            continue;
        }

        line += i;

        if (strlen(line) < 7) {
            /* There aren't enough numbers for a valid rgb color */
            continue;
        }

        /* Now the line looks like:
               *.colorname: #000000\n
                            ^
                            line
         */
        int j;
        for (j = 0; j < 3; j++) {
            char colorstr[3];
            snprintf(colorstr, 3, "%c%c", line[2*j+1], line[2*j+2]);

            char *endptr = NULL;
            long color = strtol(colorstr, &endptr, 16);

            if (*endptr != '\0') {
                break;
            }

            float ratio = ((float)color)/255.0f;
            output[outputptr] = ratio;
            outputptr++;
        }

        /* Allocate some memory for the next round */
        line = xmalloc(256);
        linesize = 256;
    }
    if (fclose(f)) {
        perror("fclose");
        return -1;
    }
    return 0;
}
