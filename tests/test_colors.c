#include "../colors.h"
#include "minunit.h"
#include "../rnn.h"
#include <stdio.h>

char *
test_extract_colors() {
	float colors[30];
	int res = extract_colors("tests/0001.png", colors);

	mu_assert("extract_colors failed", res == 0);

	/* Check against the output from sin's unaltered colors program */
	char expected[] = {
		0x6a,0x58,0x40,
		0xa3,0xb4,0x9e,
		0x22,0x35,0x4d,
		0x9d,0x74,0x43,
		0x3f,0x6e,0xae,
		0xbf,0x9d,0x55,
		0xa6,0xc8,0xe0,
		0x6d,0xab,0xdb,
		0xdc,0xc3,0x6f,
		0xe1,0xe8,0xf0
	};

	for (int i = 0; i < 30; i++) {
		mu_assert("extract_colors wrong",
				  expected[i] == (char)(colors[i]*255.0f));
	}
	return 0;
}

char *
test_extract_resources() {
	float colors[54];
	int res = extract_resources("tests/0001.resources", colors);

	/* If this is wrong, this output will look obviously wrong */
	print_xresources(colors);

	/* Sanity checks */
	mu_assert("extract_resources failed", res == 0);
	mu_assert("background wrong",
			  ((float)0xF7)/255.0f == colors[0]);
	mu_assert("foreground wrong",
			  ((float)0x35)/255.0f == colors[3]);
	mu_assert("color15 wrong",
			  ((float)0x45)/255.0f == colors[53]);
	return 0;
}
