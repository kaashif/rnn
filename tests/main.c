#include <stdio.h>
#include "minunit.h"
#include "test_colors.h"

int tests_run = 0;

char *
all_tests () {
	mu_run_test(test_extract_resources);
	mu_run_test(test_extract_colors);
	return 0;
}

int
main () {
	char *result = all_tests();
	if (result != 0) {
		printf("%s\n", result);
	} else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);
 
	return result != 0;
}
