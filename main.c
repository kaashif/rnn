#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "rnn.h"
#include "train.h"

int
main(int argc, char *argv[]) {
    int c;
    char *brain = NULL;
    int trainflag = 0;
    char *inputdir = NULL;
    while ((c = getopt(argc, argv, "vhtd:f:")) != -1) {
        switch (c) {
        case 'v':
            vflag = 1;
            break;
        case 't':
            trainflag = 1;
            break;
        case 'd':
            inputdir = optarg;
            break;
        case 'f':
            brain = optarg;
            break;
        case 'h':
            usage(argv[0]);
            break;
        default:
            return EXIT_FAILURE;
        }
    }
    if (trainflag) {
        if (!inputdir) {
            printf("-d is required to train\n");
            return EXIT_FAILURE;
        }
        if (!brain) {
            printf("-f is required to train\n");
            return EXIT_FAILURE;
        }
        return rnn_train(inputdir, brain);
    }
    if (!brain) {
        brain = RNN_TRAINED;
    }
    if (optind == argc) {
        printf("image input required\n");
        return EXIT_FAILURE;
    }
    char *input_file = argv[optind];

    return rnn_run(input_file, brain);
}
