#ifndef R_COLORS_H
#define R_COLORS_H

int extract_colors(char* input, float output[]);
int extract_resources(char* input, float output[]);

#endif
