#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/queue.h>
#include <string.h>
#include <floatfann.h>
#include "colors.h"
#include "rnn.h"

#define LIST_FREE(headpvar, npvar, entries) {   \
        while (!LIST_EMPTY(headpvar)) {         \
            npvar = LIST_FIRST(headpvar);       \
            LIST_REMOVE(npvar, entries);        \
            free(npvar);                        \
        }                                       \
}

struct cand {
    char *name; /* Basename of a PNG which might have a corresponding
                 * resources file */
    LIST_ENTRY(cand) entries;
};

struct datum {
    char *name; /* Basename of paired up PNG and resources file */
    float *png_colors; /* must be length 30 */
    float *resource_colors; /* must be length 54 */
    LIST_ENTRY(datum) entries;
};

/* Returns NULL if str does not end with suffix, returns a new string
 * with the suffix removed if str does end with suffix */

char *
endswith(char *str, char *suffix) {
    if (!str || !suffix) {
        return NULL;
    }
    size_t l1 = strlen(str);
    size_t l2 = strlen(suffix);

    if (l1 < l2) {
        return NULL;
    }
    if (!strcmp(str+l1-l2, suffix)) {
        char *prefix = xmalloc(l1-l2+1);
        strncpy(prefix, str, l1-l2);
        prefix[l1-l2] = '\0';
        return prefix;
    }
    return NULL;
}

/* Fills out datum using png and resources file from inputdir */

int
extract_datum(char *inputdir, struct datum *datum) {
    int ret = 0;
    char name_png[256];
    char name_resources[256];

    snprintf(name_png, 256, "%s/%s.png", inputdir, datum->name);
    name_png[255] = '\0';

    snprintf(name_resources, 256, "%s/%s.resources", inputdir, datum->name);
    name_resources[255] = '\0';

    datum->png_colors = calloc(30, sizeof(float));

    if (!datum->png_colors) {
        return -1;
    }

    if (extract_colors(name_png, datum->png_colors) < 0) {
        ret = -1;
        goto datum_png_free;
    }

    datum->resource_colors = calloc(54, sizeof(float));

    if(!datum->resource_colors) {
        ret = -1;
        goto datum_png_free;
    }

    if (extract_resources(name_resources, datum->resource_colors) < 0) {
        ret = -1;
        goto datum_resource_free;
    }

    return ret;

datum_resource_free:
    free(datum->resource_colors);
datum_png_free:
    free(datum->png_colors);
    return ret;
}

/* Properly frees the array of arrays in data and destroys it */

void
rnn_destroy_train(struct fann_train_data *data) {
    int i;
    for (i = 0; i < 30; i++) {
        free(data->input[i]);
        data->input[i] = NULL;
    }
    data->input = NULL;
    for (i = 0; i < 54; i++) {
        free(data->output[i]);
        data->output[i] = NULL;
    }
    data->output = NULL;
    fann_destroy_train(data);
}

struct fann_train_data *
generate_data(char *inputdir) {
    struct fann_train_data *ret = NULL;
    verprintf("Data directory: %s\n", inputdir);

    LIST_HEAD(, cand) candidates;
    LIST_HEAD(, datum) data;

    LIST_INIT(&candidates);
    LIST_INIT(&data);

    struct dirent **filelist;
    int nfiles = 0;

    if ((nfiles = scandir(inputdir, &filelist, NULL, alphasort)) < 0) {
        perror("scandir");
        return NULL;
    }

    int i;
    for (i = 0; i < nfiles; i++) {
        struct dirent *entry = filelist[i];
        char *prefix = NULL;
        if ((prefix = endswith(entry->d_name, ".png"))) {
            /* We have found a possible data point */

            struct cand *newcand = malloc(sizeof(struct cand));
            if (!newcand) {
                perror("malloc");
                goto data_lists_free;
            }
            newcand->name = prefix;
            LIST_INSERT_HEAD(&candidates, newcand, entries);
        } else if ((prefix = endswith(entry->d_name, ".resources"))) {
            /* We have found a resources file. Note that since
             * filelist is in alphasort order, if a corresponding PNG
             * exists, we have already found it and put it in the
             * candidates list */

            /* Try to find and pair up with a PNG candidate */
            struct cand *candp = NULL;
            LIST_FOREACH(candp, &candidates, entries) {
                if (!strcmp(candp->name, prefix)) {
                    struct datum *newdatum = malloc(sizeof(struct datum));
                    if (!newdatum) {
                        perror("malloc");
                        goto data_lists_free;
                    }
                    newdatum->name = candp->name;
                    LIST_REMOVE(candp, entries);
                    free(candp);
                    LIST_INSERT_HEAD(&data, newdatum, entries);
                    break;
                }
            }
        }
    }

    struct datum *np = NULL;
    unsigned int len = 0;

    LIST_FOREACH(np, &data, entries) {
        len++;
    }

    /* Number of data points, input 30 floats, output 54 floats */
    struct fann_train_data *fann_data = fann_create_train(len, 30, 54);

    if (!fann_data) {
        goto data_filelist_free;
    }

    int extracted = 0;

    LIST_FOREACH(np, &data, entries) {
        extracted++;
        verprintf("\rExtracting data point %d/%d", extracted, len);
        fflush(stdout);
        if (extract_datum(inputdir, np) < 0) {
            rnn_destroy_train(fann_data);
            goto data_filelist_free;
        }

        /* Manipulating fann_train_data directly is kind of a hack,
         * since the docs say one "should never" do it */
        fann_data->input[extracted-1] = np->png_colors;
        fann_data->output[extracted-1] = np->resource_colors;
    }
    verprintf("\n");

    ret = fann_data;

data_filelist_free:
    for (int k = 0; k < nfiles; k++) {
        free(filelist[i]);
    }
    free(filelist);
data_lists_free:
    np = NULL;
    LIST_FREE(&data, np, entries);
    struct cand *candp = NULL;
    LIST_FREE(&candidates, candp, entries);
    return ret;
}

/* Use the data points in inputdir and train a neural network, dumping
 * it to the output file.
 */

int
rnn_train(char *inputdir, char *output) {
    int ret = EXIT_SUCCESS;
    struct fann_train_data *data = generate_data(inputdir);

    if (!data) {
        return EXIT_FAILURE;
    }

    /* This following block is taken directly from the libfann docs.
     * TODO: Tweak the parameters.
     */

    const unsigned int num_input = 3*10; // 3 times 10 most used colors
    const unsigned int num_output = 3*18; // 3 times the 18 colors used in a colorscheme
    const unsigned int num_layers = 2+4;
    const float desired_error = 0.0014f; //tolerance level
    const unsigned int max_epochs = 1500000; // allow for a long run
    const unsigned int epochs_between_reports = 20000;

    struct fann *ann = fann_create_standard(num_layers, num_input,
                                            34, 40, 60, 47,
                                            num_output);
    if (!ann) {
        ret = EXIT_FAILURE;
        goto train_data_free;
    }

    fann_set_training_algorithm(ann, FANN_TRAIN_QUICKPROP);

    fann_set_activation_function_hidden(ann, FANN_ELLIOT_SYMMETRIC);
    fann_set_activation_function_output(ann, FANN_ELLIOT_SYMMETRIC);
    fann_set_activation_steepness_layer(ann, 0.34, 1);
    fann_set_activation_steepness_layer(ann, 0.3, 2);
    fann_set_activation_steepness_layer(ann, 0.5, 3);
    fann_set_activation_steepness_layer(ann, 0.34, 4);
    fann_set_activation_steepness_layer(ann, 0.34, 5);

    fann_train_on_data(ann,
                       data,
                       max_epochs,
                       epochs_between_reports,
                       desired_error);

    fann_save(ann, output);

train_ann_free:
    fann_destroy(ann);
train_data_free:
    rnn_destroy_train(data);
    return ret;
}
