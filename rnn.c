#include <stdio.h>
#include <floatfann.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "rnn.h"
#include "colors.h"
#include "train.h"

int vflag = 0;

char *
colorname(int i) {
    char *ret = NULL;
    switch (i) {
    case 0:
        ret = strdup("background");
        break;
    case 1:
        ret = strdup("foreground");
        break;
    default:
    {
        ret = malloc(10);
        snprintf(ret, 10, "color%d", i-2);
    }
    }
    return ret;
}

/* Sometimes a color or two is completely wrong. This happens
 * sometimes with "weird" images where the neural network just has no
 * idea what's going on. We need to fix this so the mistakes aren't
 * too obvious.
 */

void
fix_colors(int colors[]) {
    bool normal[3];
    int nnormal = 0;
    for (int i = 0; i < 3; i++) {
        if (0 <= colors[i] && colors[i] <= 255) {
            normal[i] = true;
            nnormal++;
        }
    }
    switch (nnormal) {
    case 3:
        /* All are normal, don't worry */
        return;
    case 2:
        /* Average out the last one, it will probably be fine */
        {
            int sum = 0;
            int abnormal = 0;
            for (int j = 0; j < 3; j++) {
                if (normal[j]) {
                    sum += colors[j];
                } else {
                    abnormal = j;
                }
            }
            colors[abnormal] = sum/2;
        }
        break;
    case 1:
        /* Set them all to be the same? */
        {
            int inormal = 0;
            for (int j = 0; j < 3; j++) {
                if (normal[j]) {
                    inormal = j;
                }
            }
            for (int j = 0; j < 3; j ++) {
                colors[j] = colors[inormal];
            }
        }
        break;
    default:
        /* Completely gone, just leave it */
        break;
    }
}

void
print_xresources(float out[]) {
    int i;
    for (i = 0; i < 18; i++) {
        char *cname = colorname(i);
        if (!cname) {
            exit(EXIT_FAILURE);
        }
        int colors[3];
        for (int j = 0; j < 3; j++) {
            colors[j] = (int)(out[3*i+j]*255.0f);
        }
        fix_colors(colors);
        printf("*.%s: #%02x%02x%02x\n", cname, colors[0], colors[1], colors[2]);
        free(cname);
    }
}

void
usage(char *name) {
    printf("usage: %s [-tv] [-d inputdir] [-f brain] [image.png]\n", name);
}

/* Run a pre-trained brain on an image file input, print the resources
 * output to stdout.
 */

int
rnn_run(char *input_file, char *brain) {
    float input[30];
    if (extract_colors(input_file, input) == -1) {
        return EXIT_FAILURE;
    }

    struct fann *ann = fann_create_from_file(brain);
    if (!ann) {
        return EXIT_FAILURE;
    }

    fann_type *out = fann_run(ann, input);
    if (!out) {
        return EXIT_FAILURE;
    }

    print_xresources(out);

    free(out);
    fann_destroy(ann);

    return EXIT_SUCCESS;
}

/* malloc wrapper, guaranteed to succeed. The idea is that we save all
 * user state to disk to avoid borking configs due to a segfault, for
 * example. Currently there is no such state, so we just exit.
 */

void *
xmalloc(size_t n) {
    void *ret = malloc(n);
    if (!ret) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    return ret;
}
